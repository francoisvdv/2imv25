/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continousfilereader;

import communication.AngleClient;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Calculates the angle \alpha and \beta based on the CppMT output data. camera
 * alpha is at (0,0). Camera beta is at (xOffsetBeta,0)
 *
 * @author Max Sondag
 */
public class AngleUpdater extends ContinousFileReader {

    String inputFileLocation = "C:\\Development\\ASMS_tracker\\ASMS_tracker\\Debug\\output.txt";
    String outputFileLocation = "C:\\Development\\ASMS_tracker\\ASMS_tracker\\Debug\\angleOutput.txt";
    PrintWriter writer;
    AngleClient server;

    boolean isAlpha = false;//whether we are getting the "alpha" or "beta" camera
    //alpha is leftbottom, beta rightbottom

    //TODO VERIFY ALL THESE NUMBERS. INCLUDING SCREENWITH
    
    //laptop max: 58.6 Macbook:60.33
    double viewAngleAlpha = 60.33;//the amount of degrees camera alpha can view
    
    //maccbook
    double viewAngleBeta = 58.6;//the amount of degrees camera beta can view
    
    
    int viewAngleAlphaOffset = 45;//the amount of degrees the rightmost viewpoint 
    //of the alpha camera is off from the line going through (0,0),(xOffsetBeta,0)
    int viewAngleBetaOffset = 45;//the amount of degrees the leftmost viewpoint 
    //of the beta camera is off from the line going through (xOffsetBeta,0),(0,0)
    int imageWidth = 600;//the width of the CppMt window,
    //Should always be between 0 and 90
    int xOffsetBeta = 185;//the offset of camera beta compared to camera alpha

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            AngleUpdater locationUpdate = new AngleUpdater();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContinousFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(AngleUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AngleUpdater() throws FileNotFoundException, InterruptedException {
        try {
            InetAddress ip = InetAddress.getByName("131.155.246.157");
            int portnumber = 6790;
            server = new AngleClient(portnumber, ip);
        } catch (IOException ex) {
            Logger.getLogger(AngleUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        writer = new PrintWriter(new FileOutputStream(outputFileLocation, true));
        this.readFile(inputFileLocation);
    }

    @Override
    public void processLine(String line) {
        //skip the header line
        if (line.equals("")) {
            return;
        }
        //first double contains leftmost x, 3'rd double contains with
        String trimmedLine = line;
        double startX = Double.parseDouble(trimmedLine.substring(0, trimmedLine.indexOf(",")));
        trimmedLine = trimmedLine.substring(trimmedLine.indexOf(",") + 1);
        trimmedLine = trimmedLine.substring(trimmedLine.indexOf(",") + 1);
        double xWidth = Double.parseDouble(trimmedLine.substring(0, trimmedLine.indexOf(",")));

        //calculate angle based on location
        double imageX = startX + xWidth / 2;

        double angleA = -1;
        double angleB = -1;
        if (isAlpha) {
            angleA = (imageWidth - imageX) / imageWidth * viewAngleAlpha + viewAngleAlphaOffset;
        } else {//IsAlpha = false
            angleB = (imageX) / imageWidth * viewAngleBeta + viewAngleBetaOffset;
        }

        String angle = anglesToCSV(angleA, angleB);
        System.out.println("angle = " + angle);
        try {
            server.sendCoordinates(angle);
        } catch (IOException ex) {
            Logger.getLogger(AngleUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        //write the angle to a file for future reference
        writeAngle(angle);

    }

    /**
     * If alpha or beta == -1, then it will be printed as null; Format is :
     *
     * angleA;angleB
     *
     * @param angleA
     * @param angleB
     */
    private String anglesToCSV(double angleA, double angleB) {
        String alphaAString = null;
        String betaBString = null;
        if (angleA != -1) {
            alphaAString = Double.toString(angleA);
        }
        if (angleB != -1) {
            betaBString = Double.toString(angleB);
        }
        return (alphaAString + ";" + betaBString);

    }

    public void writeAngle(String coordinates) {
        writer.println(coordinates);
        writer.flush();
    }
}
