/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package continousfilereader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Max Sondag
 */
public abstract class ContinousFileReader {

    /**
     * Continously reads the specified file and process it line by line using processLine()
     * @param fileLocation
     * @throws FileNotFoundException 
     */
    public void readFile(String fileLocation) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(fileLocation));
        String line;
        while (true) {
            try {
                line = br.readLine();
                if (line != null) {
                    processLine(line);
                } else {
                    try {
                        Thread.sleep(1);   // delay to make sure it does not use all of the cpu
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ContinousFileReader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ContinousFileReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public abstract void processLine(String line);

}
