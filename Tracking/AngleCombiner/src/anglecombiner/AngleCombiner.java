/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anglecombiner;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import communication.AngleServer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Combines the lastAngleA and lastAngleB camera angles to calculate the
 * position of the tracked object. waits to receive both an lastAngleA value and
 * a lastAngleB value before it updates
 *
 * @author Max Sondag
 */
public class AngleCombiner {

    boolean angleAUpdated = false;
    boolean angleBUpdated = false;

    double lastAngleA;
    double lastAngleB;

    int xOffsetB = 180;//the offset of camera beta compared to camera alpha

    //used to write the output to a file so that unity can use it
    PrintWriter writer;
    String outputFileLocation = "xyoutput.csv";
    PrintWriter writer2;
    String outputFileLocationTest = "xyoutputTest.csv";

    public static void main(String[] args) throws FileNotFoundException {
        AngleCombiner ac = new AngleCombiner();
        Thread t1 = new Thread() {
            @Override
            public void run() {
                try {
                    new AngleServer(ac, 6789);
                } catch (IOException ex) {
                    Logger.getLogger(AngleCombiner.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                try {
                    new AngleServer(ac, 6790);
                } catch (IOException ex) {
                    Logger.getLogger(AngleCombiner.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        t1.start();
        t2.start();
    }

    public AngleCombiner() throws FileNotFoundException {
        writer = new PrintWriter(new FileOutputStream(outputFileLocation, true));
        writer2 = new PrintWriter(new FileOutputStream(outputFileLocationTest, true));
    }

    public synchronized void updateAngleA(double angleA) {
        this.lastAngleA = Math.toRadians(angleA);
        angleAUpdated = true;
        updateIfNeeded();
    }

    public synchronized void updateAngleB(double angleB) {
        angleBUpdated = true;
        this.lastAngleB = Math.toRadians(angleB);
        updateIfNeeded();
    }

    /**
     * Updates the location if both the lastAngleA and the lastAngleB are
     * updated
     */
    private synchronized void updateIfNeeded() {
        if (angleAUpdated == false || angleBUpdated == false) {
            return;
        }
        //both angleA and angleB true
        angleAUpdated = false;
        angleBUpdated = false;
        
        
        double y = xOffsetB*(Math.sin(lastAngleA)*Math.sin(lastAngleB))/(Math.sin(lastAngleA+lastAngleB));
        double x = y/Math.tan(lastAngleA);
        writer2.println(lastAngleA + "," + lastAngleB);
        writer2.flush();
        
        writer.println(x + "," + y);
        writer.flush();
    }

        //Used for testing purposes
//        double x;
//        double y;
//        xOffsetB = 200;
//        updateAngleA(45);
//        updateAngleB(45);
//        //should be 100,100
//        double aRadians = Math.toRadians(lastAngleA);
//        double bRadians = Math.toRadians(lastAngleB);
//        
//        x = (Math.tan(bRadians) * xOffsetB) / (Math.tan(aRadians) + Math.tan(bRadians));
//        y = Math.tan(aRadians) * x;
//        
//        updateAngleA(40);
//        updateAngleB(70);
//        aRadians = Math.toRadians(lastAngleA);
//        bRadians = Math.toRadians(lastAngleB);
//        //should be around 150,128
//        x = (Math.tan(bRadians) * xOffsetB) / (Math.tan(aRadians) + Math.tan(bRadians));
//        y = Math.tan(aRadians) * x;
}
