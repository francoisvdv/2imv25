/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import anglecombiner.AngleCombiner;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AngleServer {

    String sentence;
    BufferedReader inFromUser;
    ServerSocket serverSocket;
    Socket connectionSocket;
    AngleCombiner ac;
    int portNumber;

    public AngleServer(AngleCombiner ac, int portNumber) throws IOException {
        this.ac = ac;
        this.portNumber = portNumber;
        getInputStream();
    }

    public void getInputStream() throws IOException {
        System.out.println("Waiting for socket");
        createServerSocket();
        System.out.println("Socket succefully created");
        //Only needed when we want to communicate something back. 
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

        while (true) {
            while ((sentence = inFromClient.readLine()) == null) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(AngleServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (sentence.startsWith("null")) {
                double angleB = Double.parseDouble(sentence.substring(sentence.indexOf(";") + 1));
                ac.updateAngleB(angleB);
            } else {
                double angleA = Double.parseDouble(sentence.substring(0, sentence.indexOf(";")));
                ac.updateAngleA(angleA);
            }
        }
    }

    private void createServerSocket() {
        try {
            //ugly hack
            serverSocket = new ServerSocket(portNumber);
            connectionSocket = serverSocket.accept();
        } catch (IOException ex) {
            Logger.getLogger(AngleServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
