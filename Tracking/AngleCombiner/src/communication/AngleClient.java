/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AngleClient {

    Socket clientSocket;
    DataOutputStream outToClient;

    public AngleClient(int portNumber, InetAddress ip) throws IOException {
        createClientSocket(portNumber, ip);
        outToClient = new DataOutputStream(clientSocket.getOutputStream());
    }

    private void createClientSocket(int portnumber, InetAddress ipAdress) {
        try {
            clientSocket = new Socket(ipAdress, portnumber);
        } catch (IOException ex) {
            Logger.getLogger(AngleClient.class.getName()).log(Level.SEVERE, null, ex);
            createClientSocket(portnumber, ipAdress);
        }

    }

    public void sendCoordinates(String coordinates) throws IOException {
        String sendToClient = coordinates + "\r\n";
        //Send coordinates back to client.
        outToClient.writeBytes(sendToClient);
        outToClient.flush();
    }
}
