﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TrackingController : MonoBehaviour {

	string fileName = "/Users/francois/Downloads/Telegram Desktop/xyoutput.csv";

	long bytesRead = 0;
	int newLineBytes = System.Environment.NewLine.Length;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		using (FileStream fs = new FileStream(fileName, 
		                                      FileMode.Open, 
		                                      FileAccess.Read,    
		                                      FileShare.ReadWrite))
		{
			fs.Seek(bytesRead, SeekOrigin.Begin);
			using (StreamReader sr = new StreamReader(fs))
			{
				string newLine = null;
				while((newLine = sr.ReadLine()) != null) {					
					bytesRead += newLine.Length + newLineBytes;
					Debug.Log (newLine);
				}
			}
		}
	}
}
